{stdenv, fetchFromGitHub, spdx-license-list-data, wdiff}:

with stdenv.lib;
stdenv.mkDerivation rec {
  rev = "3fb2da3a673c1a837d9e22646291176df5d9649a";
  version = "0.0.0.git.${rev}";
  repo =  "spdx-license-match";
  name = "${repo}-${version}";

  src = fetchFromGitHub {
    inherit repo rev;
    owner = "rohieb";
    sha256 = "0924kn9zavdxx53ijmk1pmpzjd6khfv7k0brp4qn3f9x35jwjh49";
  };

  buildInput = [ spdx-license-list-data wdiff ];

  patches = [ ./fix.patch ];

  postPatch = ''
    substituteInPlace spdx-license-match \
      --replace 'LICENSE_DIR=''$BINDIR/license-list-data' 'LICENSE_DIR=${spdx-license-list-data}' \
      --replace 'wdiff' '${wdiff}/bin/wdiff'
  '';

  installPhase = ''
    mkdir -pv $out/bin
    cp -v spdx-license-match $out/bin
  '';

  meta = {
    description = "Match text against SPDX-known licenses";
    homepage = "https://github.com/rohieb/spdx-license-match";
    platforms = platforms.all;
    license = {
      fullName = "BSD Zero Clause License";
      url = "https://spdx.org/licenses/0BSD";
    };
  };
}
