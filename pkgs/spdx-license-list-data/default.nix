{stdenv, fetchurl, git}:

with stdenv.lib;
stdenv.mkDerivation rec {
  version = "3.5";
  name = "spdx-license-list-data-${version}";

  src = fetchurl {
    url = "https://github.com/spdx/license-list-data/archive/v${version}.tar.gz";
    sha256 = "0q8swyg64nagvz9ncql2i5phgwj0zm0yqf9c29q6j4d7qgwayrjn";
  };

  installPhase = ''
    mkdir -pv $out/template
    cp -v template/*.txt $out/template
  '';

  meta = {
    description = "SPDX License List";
    homepage = "https://github.com/spdx/license-list";
    platforms = platforms.all;
    license = licenses.unlicense;
  };
}
