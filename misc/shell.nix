# This file may help developing packages

# Copy it next to a default.nix you want to troubleshoot and run `nix-shell`.
# This will drop you into a nix shell with all build dependencies installed.
# All commands to trigger package creation phases are now available.

{ pkgs ? import <nixpkgs> {} }:

with pkgs;

{
  this = callPackage ./default.nix { };
}
