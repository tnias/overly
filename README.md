# Overly Overlay

A nixpkgs overlay.


## Install

1. Clone git repository
2. Run `setup.sh`, this will set symlinks in `~/.config/nixpkgs/overlays`
3. Enjoy


## References

- [official nixpkgs manual](https://nixos.org/nixpkgs/manual)
- [nixpkgs-mozilla overlay](https://github.com/mozilla/nixpkgs-mozilla)
