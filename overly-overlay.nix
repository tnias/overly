self: super:

let
  inherit (super) callPackage;
in {
  spdx-license-list-data = callPackage ./pkgs/spdx-license-list-data {};
  spdx-license-match = callPackage ./pkgs/spdx-license-match {};
  stagit = callPackage ./pkgs/stagit {};
}
